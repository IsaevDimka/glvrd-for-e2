
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Glvrd plugin demo</title>
        <!-- Make sure the path to CKEditor is correct. -->
        <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="glvrdPlugin/plugin.js"></script>
    </head>
    <style>

        .rows{
            float:left;
        }
        .row8{
            width:60%;
        }
        .row4{
            width:30%;
        }
    </style>
    <body>
    <div id="ckeditor" class="row8 rows">
        <form>
            <textarea name="editor1" id="editor1" rows="10" cols="80">
Ивата работал в сфере создания видеоигр с 1980-х годов, свою карьеру он начал в компании HAL Laboratory. В 1993 году Ивата занял позицию президента HAL Laboratory, а в 2000-м занял в Nintendo должность главы департамента корпоративного планирования. В 2002 году Ивата стал президентом Nintendo (на этом посту он сменил Хироси Ямаути, который возглавлял Nintendo с 1949 года). Ивата принимал участие в разработке игры о водопроводчике Марио.
            </textarea>
            <script>
                CKEDITOR.replace( 'editor1' );
            </script>
        </form>
    </div>
    <div id="glvrd_results" class="row4 rows">
        <h1 id="glvrd_name"></h1>
        <div id="glvrd_description"></div>
    </div>
    </body>
</html>
