=== Russian texts proofreader Glvrd ===
Contributors: lopinopulos
Donate link: http://lopinopulos.com/
Tags: admin, posts, text, proofread, russian
Requires at least: 4.2
Tested up to: 4.3.1
Stable tag: 1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Plugin will proofread posts and pages written in russian. Proofread mechanism is provided by service glvrd.ru. Requires connect to glvrd.ru

== Description ==

The plugin will proofread russian texts in TinyMCE Wordpress editor. After plugin activation, the new button will be added to the editor's toolbar and a box with results will appear under the editor. 

First proofread will take place after the button clicked. Next proofreads will be made automatically.

== Installation ==

1. Upload Glvrd proofreader to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Use the pipe button with orange background located at the post editor's toolbar to proofread your text.

== Screenshots ==

1. Proofreaded text with advice box right near the word

== Changelog ==

= 1.2 =
* Rules is shown in tooltips instead of box below the editor
* HTTPS library download
* Cleaner HTML markup in HTML editor

= 1.1 =
* Fix loosing of the caret position after proofread

= 1.0 =
* Initial version

== Upgrade Notice ==

= 1.2 =
Require Wordpress 4.2

= 1.1 =
In this version your caret will not disappear after the proofread

== Arbitrary section ==

Glvrd.ru service was written by Maxim Ilyahov(maximilyahov.ru) and Anatoly Burov(http://anatolyburov.ru/). If you want to develop new plugins for that service, please, check the website https://api.glvrd.ru/ for API or contact admin@glvrd.ru